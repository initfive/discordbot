import discord
import logging
import logging.handlers
import os
import yt_dlp
from dotenv import load_dotenv
from discord_webhook import DiscordWebhook

load_dotenv()

logger = logging.getLogger('discord')
logger.setLevel(os.getenv('LOG_LEVEL', logging.INFO))
logging.getLogger('discord.http').setLevel(logging.INFO)

handler = logging.handlers.RotatingFileHandler(
    filename='logs/discord.log',
    encoding='utf-8',
    maxBytes=32 * 1024 * 1024,  # 32 MiB
    backupCount=5,  # Rotate through 5 files
)
dt_fmt = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter('[{asctime}] [{levelname:<8}] {name}: {message}', dt_fmt, style='{')
handler.setFormatter(formatter)
logger.addHandler(handler)

logger.info("Starting Discord bot")
DISCORD_WEBHOOK_URL = os.getenv('DISCORD_WEBHOOK_URL')
logger.debug(f"Discord webhook URL: {DISCORD_WEBHOOK_URL}")
DISCORD_CLIENT_TOKEN = os.getenv('DISCORD_CLIENT_TOKEN')
logger.debug(f"Discord client token: {DISCORD_CLIENT_TOKEN}")

emotes = {
    "<:1_:899325474541543436>": "images/1.png",
    "<:2_:899325465028874261>": "images/5.png",
    "<:3_:899325462554222642>": "images/3.png",
    "<:4_:899325468019400755>": "images/2.png",
    "<:5_:899325466882756639>": "images/4.png",
    "<:6_:899325469252534273>": "images/6.png",
    "<:7_:899325468120080514>": "images/7.png",
    "<:8_:899325474516389889>": "images/8.png",
    "<:9_:899325466886959124>": "images/9.png",
    "<:10:>": "images/10.png",
    "<:11:899325469118304318>": "images/11.png",
    "<:12:899325472213696534>": "images/12.png",
    "<:13:899325468518526996>": "images/13.png",
    "<:14:899325472012374027>": "images/14.png",
    "<:15:899325467348303913>": "images/15.png",
    "<:16:899325464408117340>": "images/16.png",
    "<:17:899325473962737735>": "images/17.png",
    "<:19:899325472721223721>": "images/19.png",
    "<:20:899325468401094776>": "images/20.png",
    "<:21:899325468510142485>": "images/21.png",
    "<:22:899325474679955526>": "images/22.png",
    "<:23:899325473094500353>": "images/23.png",
    "<:24:899325474470232085>": "images/24.png",
    "<:25:899325474549923920>": "images/25.png",
    "<:26:899325474008870922>": "images/26.png",
    "<:27:899325468585623553>": "images/27.png",
    "<:28:899325472712839198>": "images/28.png",
    "<:29:899325468933750794>": "images/29.png",
    "<:30:899325469848125520>": "images/30.png",
    "<:31:899325473597837332>": "images/31.png",
    "<:32:899325473325203477>": "images/32.png",
    "<:33:899325475493642350>": "images/33.png",
    "<:34:899325473388134420>": "images/34.png",
    "<:35:899325471823638549>": "images/35.png",
    "<:36:899325474239578133>": "images/36.png",
    "<:37:899325474709323856>": "images/37.png",
    "<:38:899325474575106128>": "images/38.png",
    "<:39:899325473279074315>": "images/39.png",
    "<:40:899325466660438076>": "images/40.png",
    "<:41:899325472117252156>": "images/41.png",
    "<:42:899325475292319754>": "images/42.png",
    "<:43:899325466748551189>": "images/43.png",
    "<:niels:1048607189553651763>": "images/niels.png"
}

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

logger.info("Discord bot started")

patterns = [
    r"https?://(www\.)?twitter\.com/",
    r"https?://(www\.)?x\.com/",
    r"https?://(www\.)?instagram\.com/"
]

# Create a function which will use yt-dlp to download an Instagram video, store locally, and send to Discord
def download_instagram_media(message, type):
    url = message.content
    logger.debug(f"Instagram media URL: {url}")
    ydl_opts = {
        'outtmpl': 'videos/%(id)s.%(ext)s',
        'format': 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best',
        'quiet': True
    }
    if type == "reel":
        try:
            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                info = ydl.extract_info(url, download=True)
                logger.debug(f"Video info: {info}")
                video_id = info.get('id', None)
                logger.debug(f"Video ID: {video_id}")
                video_file = f"videos/{video_id}.mp4"
                logger.debug(f"Video file: {video_file}")
                webhook = DiscordWebhook(
                    url=DISCORD_WEBHOOK_URL, 
                    username = message.author.name,
                    avatar_url = str(message.author.avatar.url))
                with open(video_file, "rb") as f:
                    webhook.add_file(file=f.read(), filename = video_file)
                response = webhook.execute()
                logger.debug(f"Webhook response: {response}")
                os.remove(video_file)
                
        except Exception as e:
            logger.error(f"Error in download_instagram_media: {str(e)}")
            raise e

@client.event
async def on_message(message):
    logger.debug(f"Message from {message.author}: {message.content}")
    if not message.author.bot:
        if message.content in emotes.keys():
            logger.debug(f"Emote found in message: {message.content}")
            try:
                webhook = DiscordWebhook(
                    url=DISCORD_WEBHOOK_URL, 
                    username = message.author.name,
                    avatar_url = str(message.author.avatar.url))

                file = (emotes[message.content])
                logger.debug(f"Emote file: {file}")
                with open(file, "rb") as f:
                    webhook.add_file(file=f.read(), filename = file)
            
                response = webhook.execute()
                await message.delete()
            except KeyError:
                return

        if any(substring in message.content for substring in [
            "://twitter.com/", 
            "://www.twitter.com/", 
            "://x.com", 
            "://www.x.com", 
            "://instagram.com/", 
            "://www.instagram.com/"
            ]):
            logger.debug(f"Media link found in message: {message.content}")
            try:
                webhook = DiscordWebhook(
                    url=DISCORD_WEBHOOK_URL, 
                    username = message.author.name,
                    avatar_url = str(message.author.avatar.url))
                # make this a regex to check on both ://twitter.com and www.twitter
                if "twitter.com" in message.content:
                    webhook.content = message.content.replace("twitter.com", "fxtwitter.com")
                elif "x.com" in message.content:
                    webhook.content = message.content.replace("x.com", "fixupx.com")
                elif "instagram.com" in message.content:
                    if message.content.startswith("https://www.instagram.com/reel"):
                        download_instagram_media(message, type="reel")
                    #elif message.content.startswith("https://www.instagram.com/p"):
                        #download_instagram_media(message, type="post")
                    else:
                        webhook.content = message.content.replace("instagram.com", "ddinstagram.com")
                logger.debug(f"Changed media URL: {webhook.content}")
                response = webhook.execute()
                logger.debug(f"Webhook response: {response}")
                await message.delete()
            except KeyError:
                return

client.run(DISCORD_CLIENT_TOKEN, log_handler=None)

