FROM python:3

WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y ffmpeg && rm -rf /var/lib/apt/lists/*
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY DiscordBot.py ./
COPY .env ./
COPY images/ images/
RUN mkdir -p logs videos images

CMD [ "python", "./DiscordBot.py" ]